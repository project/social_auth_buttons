<?php

namespace Drupal\social_auth_buttons\Services;

use Symfony\Component\Routing\RouteCollection;
use Drupal\Core\Routing\RouteProvider;

/**
 * A Route Provider front-end for all Drupal-stored routes.
 */
class SocialAuthButtonsRouteProvider extends RouteProvider {

  /**
   * Get all social auth routes.
   */
  public function getSocialAuthRoutes() {
    $collection = new RouteCollection();

    try {
      $routes = $this->connection->query("SELECT name, route, fit FROM {" . $this->connection->escapeTable($this->tableName) . "} WHERE name LIKE 'social_auth%.redirect%'", [])
        ->fetchAll(\PDO::FETCH_ASSOC);
    }
    catch (\Exception $e) {
      $routes = [];
    }

    // We sort by fit and name in PHP to avoid a SQL filesort and avoid any
    // difference in the sorting behavior of SQL back-ends.
    usort($routes, [$this, 'routeProviderRouteCompare']);

    foreach ($routes as $row) {
      $collection->add($row['name'], unserialize($row['route']));
    }

    return $collection;
  }

  /**
   * Get social auth route by network id.
   */
  public function getSocialAuthRouteByNetworkId($id) {
    try {
      $routes = $this->connection->query("SELECT name, route, fit FROM {" . $this->connection->escapeTable($this->tableName) . "} WHERE name LIKE '" . $id . ".redirect%'", [])
        ->fetchAll(\PDO::FETCH_ASSOC);
    }
    catch (\Exception $e) {
      $routes = [];
    }

    // We sort by fit and name in PHP to avoid a SQL filesort and avoid any
    // difference in the sorting behavior of SQL back-ends.
    usort($routes, [$this, 'routeProviderRouteCompare']);

    foreach ($routes as $row) {
      return unserialize($row['route']);
    }

    return NULL;
  }

}
