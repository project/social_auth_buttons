<?php

namespace Drupal\social_auth_buttons\Element;

use Drupal\Component\Utility\Html as HtmlUtility;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\Core\Render\Element\RenderElement;

/**
 * Provides a render element that wraps child elements in a container.
 *
 * Surrounds child elements with a <div> and adds attributes such as classes or
 * an HTML ID.
 *
 * Properties:
 * - #optional: Indicates whether the container should render when it has no
 *   visible children. Defaults to FALSE.
 *
 * Usage example:
 * @code
 * $form['needs_accommodation'] = [
 *   '#type' => 'checkbox',
 *   '#title' => $this->t('Need Special Accommodations?'),
 * ];
 *
 * $form['accommodation'] = [
 *   '#type' => 'social_auth_buttons',
 *   '#attributes' => [
 *     'class' => ['accommodation'],
 *   ],
 *   '#states' => [
 *     'invisible' => [
 *       'input[name="needs_accommodation"]' => ['checked' => FALSE],
 *     ],
 *   ],
 * ];
 *
 * $form['accommodation']['diet'] = [
 *   '#type' => 'textfield',
 *   '#title' => $this->t('Dietary Restrictions'),
 * ];
 * @endcode
 *
 * @RenderElement("social_auth_buttons")
 */
class SocialAuthButtons extends RenderElement {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $class = get_class($this);
    return [
      '#optional' => FALSE,
      '#process' => [
        [$class, 'processContainer'],
      ],
      '#theme' => 'social_auth_buttons',
    ];
  }

  /**
   * Processes a container element.
   *
   * @param array $element
   *   An associative array containing the properties and children of the
   *   container.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param array $complete_form
   *   The complete form structure.
   *
   * @return array
   *   The processed element.
   */
  public static function processContainer(array &$element, FormStateInterface $form_state, array &$complete_form) {
    // Generate the ID of the element if it's not explicitly given.
    if (!isset($element['#id'])) {
      $element['#id'] = HtmlUtility::getUniqueId(implode('-', $element['#parents']) . '-wrapper');
    }

    $container = \Drupal::getContainer();
    $plugin_manager = $container->get('plugin.network.manager');
    $networks = $plugin_manager->getDefinitions();
    $type = 'social_auth';

    $route_provider = \Drupal::service('social_auth_buttons.route_provider');

    $element['items'] = [];
    foreach ($networks as $network) {
      if ($network['type'] == $type) {
        $route = $route_provider->getSocialAuthRouteByNetworkId($network['id']);

        $element['items'][$network['id']] = [
          '#type' => 'social_auth_buttons_link',
          '#link' => Link::fromTextAndUrl($network['social_network'], Url::fromUserInput($route->getPath())),
          '#name' => $network['id'],
          '#network' => $network,
        ];
      }
    }

    return $element;
  }

}
